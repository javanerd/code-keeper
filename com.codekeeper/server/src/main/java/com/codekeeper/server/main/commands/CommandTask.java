/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.main.commands;

import java.util.Scanner;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.server.main.interfaces.ICodeServer;
import com.codekeeper.server.main.interfaces.ICommand;
import com.codekeeper.server.main.interfaces.ICommandListener;
import com.codekeeper.shared.util.Constants;

/**
 * Command task.
 *
 * @author fredladeroute
 */
public class CommandTask extends TimerTask {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(CommandTask.class);

    /**
     * Scanner for gathering user input.
     */
    private static Scanner scanner = new Scanner(System.in);

    /**
     * Reference to the parent server.
     */
    private final ICodeServer serverReference;

    /**
     * Command listener reference.
     */
    private final ICommandListener commandListener;

    /**
     * Stop command.
     */
    private static final String STOP_CMD = "stop";

    /**
     * Start command.
     */
    private static final String START_CMD = "start";

    /**
     * Exit command.
     */
    private static final String EXIT_CMD = "exit";

    /**
     * Construct a CommandTask.
     *
     * @param serverRef
     *            a reference to the server
     * @param listener
     *            the command listener reference
     */
    public CommandTask(final ICodeServer serverRef,
            final ICommandListener listener) {
        this.serverReference = serverRef;
        this.commandListener = listener;
    }

    @Override
    public final void run() {
        final ICommand command = listen();
        LOGGER.debug("Listening for command...");
        if (command != null) {
            int timeout = Constants.COMMAND_TIMEOUT;
            while (!command.execute() && (timeout > 0)) {
                LOGGER.debug("Last attempt failed. Retrying " + --timeout
                        + " more times.");
            }

            if (timeout == 0) {
                LOGGER.error("Could not execute server command.");
            }
        }
    }

    /**
     * Listen for a command.
     *
     * @return the command called.
     */
    public final ICommand listen() {
        if (scanner.hasNextLine()) {
            final String cmd = scanner.nextLine();
            if (cmd.equals(STOP_CMD)) {
                return new ServerStopCommand(serverReference);
            } else if (cmd.equals(START_CMD)) {
                return new ServerStartCommand(serverReference, commandListener);
            } else if (cmd.equals(EXIT_CMD)) {
                return new ExitCommand(serverReference, commandListener);
            }
        }
        return new NullCommand();
    }
}
