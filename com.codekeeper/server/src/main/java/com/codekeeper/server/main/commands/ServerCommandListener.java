/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.main.commands;

import java.util.Timer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.server.main.interfaces.ICodeServer;
import com.codekeeper.server.main.interfaces.ICommand;
import com.codekeeper.server.main.interfaces.ICommandListener;
import com.codekeeper.shared.util.Constants;

/**
 * Listen for commands from the user on the command line.
 *
 * @author fredladeroute
 */
public class ServerCommandListener implements ICommandListener {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ServerCommandListener.class);

    /**
     * Reference to the parent server.
     */
    private final ICodeServer serverReference;

    /**
     * Underlying timer task.
     */
    private volatile Timer timer = new Timer();

    @Override
    public final Timer getTimer() {
        return timer;
    }

    /**
     * The command task used by the timer task.
     */
    private CommandTask task = null;

    /**
     * True if this command listener is started.
     */
    private boolean isStarted = false;

    /**
     * Server command listener constructor.
     * Constructs a new scanner for scanning for user commands.
     *
     * @param serverRef
     *            a reference to the ICodeServer
     */
    public ServerCommandListener(final ICodeServer serverRef) {
        super();
        this.serverReference = serverRef;
    }

    @Override
    public final synchronized void runCommand(final ICommand command) {
        if (isStarted) {
            if (command != null) {
                int timeout = Constants.COMMAND_TIMEOUT;
                while (!command.execute() && (timeout > 0)) {
                    LOGGER.error("Last attempt failed. Retrying " + --timeout
                            + " more times.");
                }
                if (timeout == 0) {
                    LOGGER.error("Could not execute server command.");
                }
            }
        } else {
            LOGGER.error("CommandListener not started.");
        }
    }

    @Override
    public final synchronized void start() {
        if (!isStarted) {
            task = new CommandTask(serverReference, this);
            timer.scheduleAtFixedRate(task, 2, Constants.COMMAND_POLL_DELAY);
            LOGGER.debug("Command listener started.");
            isStarted = true;
        }
    }

    @Override
    public final synchronized void stop() {
        if (isStarted) {
            LOGGER.debug("Command listener stopping.");
            task.cancel();
            task = null;
            LOGGER.debug("Command listener stopped.");
            isStarted = false;
        }
    }

    @Override
    public final synchronized void restart() {
        this.stop();
        this.start();
    }

}
