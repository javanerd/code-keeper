/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.main.interfaces;

/**
 * Command class interface.
 *
 * @author fredladeroute
 */
public interface ICommand {

    /**
     * Execute the command.
     *
     * @return exit status of the command true for success false for fail.
     */
    boolean execute();
}
