/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.security;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.server.exceptions.ServerException;

/**
 * Tools to encode and decode data to and from Base64.
 *
 * @author fredladeroute
 */
public final class Base64Tools {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(Base64Tools.class);

    /**
     * Encoding.
     */
    public static final String ENCODING = "UTF-8";

    /**
     * Unused.
     */
    private Base64Tools() {
    }

    /**
     * Encode input string into Base64.
     *
     * @param input
     *            string to be encoded
     * @return a Base64 encoded string
     */
    public static synchronized String encode(final String input) {

        if (input.isEmpty()) {
            throw new ServerException("Cannot encode, input string was empty.");
        }

        String b64String = "";

        try {
            b64String = Base64.encodeBase64String(input.getBytes(ENCODING));
        } catch (final Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return b64String;
    }

    /**
     * Decode input string from Base64 to plaintext.
     *
     * @param input
     *            Base64 encoded string
     * @return a string converted from Base64 into plaintext
     */
    public static synchronized String decode(final String input) {

        if (input.isEmpty()) {
            throw new ServerException("Cannot decode, input string was empty.");
        }
        String string = "";
        byte[] output = null;
        try {
            output = Base64.decodeBase64(input);
            string = new String(output, ENCODING);
        } catch (final Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return string;
    }
}
