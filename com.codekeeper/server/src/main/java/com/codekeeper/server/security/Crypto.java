/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.security;

import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import com.codekeeper.shared.util.Constants;

/**
 * Class to encrypt and decrypt strings.
 *
 * @author fredladeroute
 */
public class Crypto {

    /**
     * String encryptor.
     */
    private final StandardPBEStringEncryptor encryptor;

    /**
     * Create an object with just the passphrase from the user.
     *
     * @param password
     *            the password for the string encryptor
     */
    public Crypto(final String password) {
        Security.addProvider(new BouncyCastleProvider());
        encryptor = new StandardPBEStringEncryptor();
        encryptor.setProvider(new BouncyCastleProvider());
        encryptor.setAlgorithm(Constants.CRYPTO_ALGORITHM);
        encryptor.setPassword(password);
    }

    /**
     * First encode the string s into Base64 then encrypt the string and return
     * the byte array containing the ciphertext.
     *
     * @param s
     *            the input string to be encrypted using AES-128
     * @return a byte array containing the ciphertext
     */
    public final String encryptString(final String s) {
        return encryptor.encrypt(s);
    }

    /**
     * Decrypt the AES-128 String and return the cleartext string.
     *
     * @param s
     *            the string to be decrypted
     * @return a cleartext version of the encrypted string s
     */
    public final String decryptString(final String s) {
        return encryptor.decrypt(s);
    }
}
