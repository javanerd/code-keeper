/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.security;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.generators.PKCS12ParametersGenerator;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.BlockCipherPadding;
import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * AES Encryption class.
 *
 * @author javanerd
 *
 */
public final class AESCrypto extends Crypto {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(AESCrypto.class);

    /**
     * Number of rounds to run.
     */
    private static final int ITER_COUNT = 32;

    /**
     * Salt value.
     */
    private byte[] salt;

    /**
     * Key size.
     */
    private static final int KEYSIZE = 256;

    /**
     * AES Block cipher.
     */
    private final BlockCipher aesCipher = new AESEngine();

    /**
     * Padded Buffer block instance.
     */
    private PaddedBufferedBlockCipher pbbc;

    /**
     * CipherParameters instance.
     */
    private ParametersWithIV key;

    /**
     * Password parameter generator.
     */
    private PBEParametersGenerator gen;

    /**
     * AES Crypto constructor.
     *
     * @param password
     *         the password for the crypto.
     * @param s
     *         the salt.
     */
    public AESCrypto(final String password, final byte[] s) {
        super(password);
        byte[] pass = PBEParametersGenerator
                .PKCS12PasswordToBytes(password.toCharArray());
        setKey(pass);
        //Utils.clear(pass);
        setPadding(new PKCS7Padding());
    }

    /**
     * Set the padding.
     * @param bcp
     *         block cipher padding.
     */
    private void setPadding(final BlockCipherPadding bcp) {
        this.pbbc = new PaddedBufferedBlockCipher(
                new CBCBlockCipher(aesCipher), bcp);
    }

    /**
     * Set the encryption key.
     * @param pass
     *         the encryption key.
     */
    private void setKey(final byte[] pass) {
        gen = new PKCS12ParametersGenerator(
                new SHA512Digest());
        gen.init(pass, salt, ITER_COUNT);
        key = (ParametersWithIV) gen.generateDerivedParameters(
                KEYSIZE, KEYSIZE / 2);
    }

    /**
     * Encrypt input data.
     *
     * @param input
     *         the data to be encrypted.
     *
     * @return
     *         the encrypted data.
     */
    public byte[] encrypt(final byte[] input) {
        return processing(input, true);
    }

    /**
     * Decrypt input data.
     *
     * @param input
     *         the cipher data to decrypt
     *
     * @return
     *         the decrypted data.
     */
    public byte[] decrypt(final byte[] input) {
        return processing(input, false);
    }

    /**
     * Process the data.
     * @param input
     *         the data to be encrypted or decrypted.
     * @param encrypt
     *         boolean true if the operation is to encrypt and false otherwise.
     * @return
     *         decrypted or cipher text data.
     */
    private byte[] processing(final byte[] input, final boolean encrypt) {

        pbbc.init(encrypt, key);

        byte[] output = new byte[pbbc.getOutputSize(input.length)];
        int offset = pbbc.processBytes(
            input, 0, input.length, output, 0);

        int last = 0;
        try {
            last = pbbc.doFinal(output, offset);
        } catch (DataLengthException e) {
            LOGGER.error(e.getMessage());
        } catch (IllegalStateException e) {
            LOGGER.error(e.getMessage());
        } catch (InvalidCipherTextException e) {
            LOGGER.error(e.getMessage());
        }

        final byte[] outdata = new byte[offset + last];
        System.arraycopy(output, 0, outdata, 0, outdata.length);
        return outdata;

    }

}
