/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.main;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;

import org.glassfish.grizzly.filterchain.FilterChainBuilder;
import org.glassfish.grizzly.filterchain.TransportFilter;
import org.glassfish.grizzly.nio.transport.TCPNIOTransport;
import org.glassfish.grizzly.nio.transport.TCPNIOTransportBuilder;
import org.glassfish.grizzly.ssl.SSLContextConfigurator;
import org.glassfish.grizzly.ssl.SSLEngineConfigurator;
import org.glassfish.grizzly.ssl.SSLFilter;
import org.glassfish.grizzly.utils.StringFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.server.filters.AuthenticationFilter;
import com.codekeeper.server.filters.DecryptionFilter;
import com.codekeeper.server.filters.LoggingFilter;
import com.codekeeper.server.filters.PairingFilter;
import com.codekeeper.server.filters.StorageFilter;
import com.codekeeper.server.filters.XmlFilter;
import com.codekeeper.server.main.interfaces.ICodeServer;
import com.codekeeper.shared.util.Constants;

/**
 * CodeServer.
 *
 * @author fredladeroute
 */
public class CodeServer implements ICodeServer {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(CodeServer.class);

    /**
     * Local transport instance.
     */
    private volatile TCPNIOTransport transport;

    /**
     * True if this server is started.
     */
    private boolean isRunning = false;

    /**
     * Coder server Constructor.
     */
    public CodeServer() {

        // Create a FilterChain using FilterChainBuilder
        final FilterChainBuilder filterChainBuilder = FilterChainBuilder
                .stateless();

        // Add TransportFilter, which is responsible
        // for reading and writing data to the connection
        filterChainBuilder.add(new TransportFilter());

        // Initialize and add SSLFilter
        final SSLEngineConfigurator serverConfig = initializeSSL();
        final SSLEngineConfigurator clientConfig = serverConfig.copy()
                .setClientMode(true);

        filterChainBuilder.add(new SSLFilter(serverConfig, clientConfig));

        // StringFilter is responsible for Buffer <-> String
        // conversion
        filterChainBuilder.add(new StringFilter(Charset
                .forName(Constants.ENCODING)));

        // EchoFilter is responsible for echoing received messages
        //filterChainBuilder.add(new EchoFilter());
        filterChainBuilder.add(new LoggingFilter());
        filterChainBuilder.add(new AuthenticationFilter());
        filterChainBuilder.add(new PairingFilter());
        filterChainBuilder.add(new DecryptionFilter());
        filterChainBuilder.add(new XmlFilter());
        filterChainBuilder.add(new StorageFilter());

        // Create TCP transport
        transport = TCPNIOTransportBuilder.newInstance().build();
        transport.setProcessor(filterChainBuilder.build());
    }

    /**
     * Initialize server side SSL configuration.
     *
     * @return server side {@link SSLEngineConfigurator}.
     */
    private static SSLEngineConfigurator initializeSSL() {
        // Initialize SSLContext configuration
        final SSLContextConfigurator sslContextConfig
                = new SSLContextConfigurator();

        // Set key store
        final ClassLoader cl = CodeServer.class.getClassLoader();
        final URL cacertsUrl = cl.getResource("ssltest-cacerts.jks");
        if (cacertsUrl != null) {
            sslContextConfig.setTrustStoreFile(cacertsUrl.getFile());
            sslContextConfig.setTrustStorePass("changeit");
        }

        // Set trust store
        final URL keystoreUrl = cl.getResource("ssltest-keystore.jks");
        if (keystoreUrl != null) {
            sslContextConfig.setKeyStoreFile(keystoreUrl.getFile());
            sslContextConfig.setKeyStorePass("changeit");
        }

        // Create SSLEngine configurator
        return new SSLEngineConfigurator(sslContextConfig.createSSLContext(),
                false, false, false);
    }

    @Override
    public final void start() {
        if (!isRunning) {
            try {
                transport.bind(Constants.HOST, Constants.PORT);
                transport.start();
                isRunning = true;
            } catch (final IOException e) {
                LOGGER.error(e.getMessage(), e);
                isRunning = false;
            }
            LOGGER.info("Server started: " + Constants.HOST);
            LOGGER.info("Listening on port: " + Constants.PORT);
            while (!transport.isStopped()) {
                try {
                    Thread.sleep(Constants.SERVER_THREAD_SLEEPTIME);
                } catch (final InterruptedException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }
    }

    @Override
    public final boolean stop() {

        if (isRunning) {
            // stop the transport
            LOGGER.info("Stopping transport...");
            try {
                transport.stop();
                isRunning = false;
                LOGGER.info("Transport stopped...");
            } catch (final IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        return !isRunning;
    }

    @Override
    public final void restart() {
        this.stop();
        this.start();
    }

}
