/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.filters;

import java.io.IOException;

import org.glassfish.grizzly.Connection;
import org.glassfish.grizzly.ReadResult;
import org.glassfish.grizzly.filterchain.BaseFilter;
import org.glassfish.grizzly.filterchain.FilterChainContext;
import org.glassfish.grizzly.filterchain.NextAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * First filter in chain, logs connections to/from the server.<br>
 * <br>
 * 1 [LoggingFilter]<br>
 * 2 AuthenticationFilter<br>
 * 3 PairingFilter<br>
 * 4 DecryptionFilter<br>
 * 5 XmlFilter<br>
 * 6 StorageFilter<br>
 * <br>
 *
 * @author fredladeroute
 */
public class LoggingFilter extends BaseFilter {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(LoggingFilter.class);

    @Override
    public final NextAction handleConnect(final FilterChainContext ctx)
            throws IOException {
        final Connection<?> connection = ctx.getConnection();
        LOGGER.info("New connection to server from: " + ctx.getAddress()
                + "\n [" + connection.read() + "]");
        return ctx.getInvokeAction();
    }

    /**
     * Handle just read operation, when some message has come and ready to be
     * processed. Log the new connection and continue.
     *
     * @param ctx
     *            Context of {@link FilterChainContext} processing
     * @return the next action
     * @throws java.io.IOException
     *             if an error occurs
     */
    @SuppressWarnings("unchecked")
    @Override
    public final NextAction handleRead(final FilterChainContext ctx)
            throws IOException {
        ReadResult<String, ?> r = ctx.read();
        LOGGER.info(r.getMessage());
        return ctx.getInvokeAction();
    }
}
