/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.main.commands;

import com.codekeeper.server.main.interfaces.ICodeServer;
import com.codekeeper.server.main.interfaces.ICommand;

/**
 * Command to stop the given server instance.
 *
 * @author fredladeroute
 */
public class ServerStopCommand extends ServerCommand implements ICommand {

    /**
     * A reference to the server.
     */
    private final ICodeServer serverReference;

    /**
     * Stop command constructor.
     *
     * @param serverRef
     *            a reference to the ICodeServer
     */
    public ServerStopCommand(final ICodeServer serverRef) {
        this.serverReference = serverRef;
    }

    @Override
    public final boolean execute() {
        log();
        return serverReference.stop();
    }

    @Override
    public final String getName() {
        return "ServerStopCommand";
    }

}
