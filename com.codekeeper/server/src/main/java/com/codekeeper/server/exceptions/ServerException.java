/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.exceptions;

/**
 * Raised when a runtime error occurs on the server.
 *
 * @author fredladeroute
 */
public class ServerException extends RuntimeException {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -2890415334327003387L;

    /**
     * Raised when a runtime error occurs on the server.
     *
     * @param cause
     *            the root cause of the problem.
     */
    public ServerException(final String cause) {
        super(cause);
    }

}
