/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.security;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Tools to get the hash of a string.
 *
 * @author fredladeroute
 */
public final class HashTools {

    /**
     * Unused.
     */
    private HashTools() {
    }

    /**
     * Return the hash of a string.
     *
     * @param data
     *            the string to get the hash of
     * @return the hash of the input string
     */
    public static synchronized String getHashData(final String data) {
        return DigestUtils.sha512Hex(data);
    }
}
