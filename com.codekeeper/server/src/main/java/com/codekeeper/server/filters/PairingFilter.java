/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.filters;

import java.io.IOException;

import org.glassfish.grizzly.filterchain.BaseFilter;
import org.glassfish.grizzly.filterchain.FilterChainContext;
import org.glassfish.grizzly.filterchain.NextAction;
import org.glassfish.grizzly.utils.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.server.enums.ServerMessage;
import com.codekeeper.shared.util.MessageUtils;

/**
 * Third filter in the chain,<br>
 * PairingFilter used to Pair the incoming string message<br>
 * with a ServerMessage.<br>
 * <br>
 * 1 LoggingFilter<br>
 * 2 AuthenticationFilter<br>
 * 3 [PairingFilter]<br>
 * 4 CommunicationFilter<br>
 * 5 DecryptionFilter<br>
 * 6 XmlFilter<br>
 * 7 StorageFilter<br>
 * <br>
 *
 * @author Fred laderoute
 */
public class PairingFilter extends BaseFilter {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(LoggingFilter.class);

    /**
     * Get the entire message from the context and remove the 2 byte header.
     * Store header and message in a pair object and invoke next filter.
     *
     * @param ctx
     *            Context of {@link FilterChainContext} processing
     * @return the next action
     * @throws java.io.IOException
     *             if an error occurs
     */
    @Override
    public final NextAction handleRead(final FilterChainContext ctx)
            throws IOException {
        final String message = ctx.<String> getMessage();
        final ServerMessage m = (ServerMessage.values())[MessageUtils
                .getHeader(message)];

        LOGGER.debug("Message recieved at Pairing filter, "
                + "servermessage status is: " + m.name());
        LOGGER.debug("Raw message is: " + message);

        final Pair<ServerMessage, String> p = new Pair<ServerMessage, String>();

        p.setFirst(m);
        p.setSecond(message.substring(1));
        ctx.setMessage(p);
        return ctx.getInvokeAction();
    }
}
