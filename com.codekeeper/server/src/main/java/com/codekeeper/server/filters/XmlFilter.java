/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.filters;

import org.glassfish.grizzly.filterchain.BaseFilter;

/**
 * Fifth filter in chain, manages parsing and validating of the<br>
 * decrypted xml file.<br>
 * <br>
 * 1 LoggingFilter<br>
 * 2 AuthenticationFilter<br>
 * 3 PairingFilter<br>
 * 4 DecryptionFilter<br>
 * 5 [XmlFilter]<br>
 * 6 StorageFilter<br>
 * <br>
 *
 * @author fredladeroute
 */
public class XmlFilter extends BaseFilter {

}
