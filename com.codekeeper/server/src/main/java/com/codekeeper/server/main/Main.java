/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.main;

import java.io.FileNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.server.main.commands.ServerCommandListener;
import com.codekeeper.server.main.commands.ServerStartCommand;
import com.codekeeper.server.main.interfaces.ICodeServer;
import com.codekeeper.server.main.interfaces.ICommandListener;
import com.codekeeper.shared.properties.IntegerPropertyManager;
import com.codekeeper.shared.util.Constants;

/**
 * Main class, application start point.
 *
 * @author fredladeroute
 */
public final class Main {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    /**
     * Server instance.
     */
    private static ICodeServer server;

    /**
     * Command listener instance used to listen for commands from the user.
     */
    private static ICommandListener listener;

    /**
     * Server start command.
     */
    private static ServerStartCommand starter = null;

    /**
     * Server property manager.
     */
    private static IntegerPropertyManager propertyManager;

    /**
     * Unused.
     */
    private Main() {
    }

    /**
     * Main method where execution begins.
     *
     * @param args
     *            command line args
     */
    public static void main(final String[] args) {
        server = new CodeServer();

        // Create a new command listener to recieve user commands.
        listener = new ServerCommandListener(server);
        starter = new ServerStartCommand(server, listener);

        addHook();

        listener.start();
        try {
            propertyManager = new IntegerPropertyManager(
                    Constants.DEFAULT_PROPERTY_FILE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (propertyManager.getProperty(
                "com.codekeeper.server.main.autostart", 0) == 1) {
            listener.runCommand(starter);
        }
    }

    /**
     * Add the shutdown hook.
     */
    private static void addHook() {
        final Thread mainThread = Thread.currentThread();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    LOGGER.info("Stopping server.");
                    server.stop();
                    LOGGER.info("Server Stopped.");
                    mainThread.join();
                    server = null;
                    System.gc();
                } catch (final InterruptedException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        });
    }
}
