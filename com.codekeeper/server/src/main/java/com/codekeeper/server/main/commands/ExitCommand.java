/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.main.commands;

import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;

import com.codekeeper.server.main.interfaces.ICodeServer;
import com.codekeeper.server.main.interfaces.ICommand;
import com.codekeeper.server.main.interfaces.ICommandListener;

/**
 * Exit the application server.
 *
 * @author fredladeroute
 */
public class ExitCommand extends ServerCommand implements ICommand {

    /**
     * A reference to the server.
     */
    private final ICommandListener commandListenerReference;

    /**
     * Reference to the server.
     */
    private final ICodeServer serverReference;

    /**
     * Exit command constructor.
     *
     * @param commandListener
     *            a reference to the ICommandListener
     * @param serverRef
     *            reference to the server
     */
    public ExitCommand(final ICodeServer serverRef,
            final ICommandListener commandListener) {
        this.serverReference = serverRef;
        this.commandListenerReference = commandListener;
    }

    @Override
    public final boolean execute() {
        serverReference.stop();
        commandListenerReference.stop();
        commandListenerReference.getTimer().cancel();

        // TODO Update when Logback fixes bug.
        // Kludge to shutdown logging framework.
        final ILoggerFactory factory = LoggerFactory.getILoggerFactory();
        if (factory instanceof LoggerContext) {
            final LoggerContext ctx = (LoggerContext) factory;
            ctx.stop();
        }
        return true;
    }

    @Override
    public final String getName() {
        return "ExitCommand";
    }

}
