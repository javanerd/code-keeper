/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.filters;

import java.security.AlgorithmParameterGenerator;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidParameterSpecException;

import javax.crypto.KeyAgreement;
import javax.crypto.spec.DHParameterSpec;

import org.glassfish.grizzly.filterchain.BaseFilter;
import org.glassfish.grizzly.filterchain.FilterChainContext;
import org.glassfish.grizzly.filterchain.NextAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.server.enums.ServerMessage;
import com.codekeeper.shared.util.Constants;

/**
 * The second filter used to setup a client to server<br>
 * session.<br>
 * <br>
 * 1 LoggingFilter<br>
 * 2 [AuthenticationFilter]<br>
 * 3 PairingFilter<br>
 * 4 DecryptionFilter<br>
 * 5 XmlFilter<br>
 * 6 StorageFilter<br>
 * <br>
 *
 * @author fredladeroute
 */
public class AuthenticationFilter extends BaseFilter {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(AuthenticationFilter.class);

    /**
     * Read data from the client for authentication.
     *
     * @param ctx
     *            the filter chain context
     * @return the next action in the filter chain
     */
    @Override
    public final NextAction handleRead(final FilterChainContext ctx) {

        DHParameterSpec dhSkipParamSpec;

        AlgorithmParameterGenerator paramGen;
        try {
            paramGen = AlgorithmParameterGenerator.getInstance("DH");
            paramGen.init(Constants.AES_DH_KEYSIZE);
            final AlgorithmParameters params = paramGen.generateParameters();
            dhSkipParamSpec = params.getParameterSpec(DHParameterSpec.class);

            LOGGER.debug("Sever: Generate DH keypair ...");
            final KeyPairGenerator serverKpairGen = KeyPairGenerator
                    .getInstance("DH");
            serverKpairGen.initialize(dhSkipParamSpec);
            final KeyPair serverKpair = serverKpairGen.generateKeyPair();

            // Server creates and initializes her DH KeyAgreement
            // object
            LOGGER.debug("Server: Initialization ...");
            final KeyAgreement serverKeyAgree = KeyAgreement.getInstance("DH");
            serverKeyAgree.init(serverKpair.getPrivate());

            // Server encodes public key, and sends it over to client.
            final byte[] serverPubKeyEnc = serverKpair.getPublic().getEncoded();
            final byte[] handshakeHeader = ServerMessage.HANDSHAKE.getAsBytes();
            final byte[] msg = new byte[handshakeHeader.length
                    + serverPubKeyEnc.length];

            // Add the handshake header to the message.
            System.arraycopy(handshakeHeader, 0, msg, 0,
                    handshakeHeader.length);

            // Add the public key to the message
            System.arraycopy(serverPubKeyEnc, 0, msg, handshakeHeader.length,
                    serverPubKeyEnc.length);

            ctx.write(ctx.getAddress(), msg, null);
        } catch (final NoSuchAlgorithmException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (final InvalidParameterSpecException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (final InvalidKeyException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (final InvalidAlgorithmParameterException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return null;

    }
}
