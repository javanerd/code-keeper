/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.main.commands;

import com.codekeeper.server.main.interfaces.ICodeServer;
import com.codekeeper.server.main.interfaces.ICommand;
import com.codekeeper.server.main.interfaces.ICommandListener;

/**
 * Command to start the server.
 *
 * @author fredladeroute
 */
public class ServerStartCommand extends ServerCommand implements ICommand {

    /**
     * A reference to the server.
     */
    private final ICodeServer serverReference;

    /**
     * Command listener reference.
     */
    private final ICommandListener commandListener;

    /**
     * Thread to run server instance on.
     */
    private Thread serverThread = null;

    /**
     * Stop command constructor.
     *
     * @param serverRef
     *            a reference to the ICodeServer
     * @param listener
     *            the command listener reference
     */
    public ServerStartCommand(final ICodeServer serverRef,
            final ICommandListener listener) {
        this.serverReference = serverRef;
        this.commandListener = listener;
    }

    @Override
    public final boolean execute() {
        log();
        commandListener.start();

        serverThread = new Thread(new Runnable() {
            @Override
            public void run() {
                serverReference.start();
            }
        });

        // Only one server thread should be running at a time.
        if (!serverThread.isAlive()) {
            serverThread.start();
        }
        return true;
    }

    @Override
    public final String getName() {
        return "ServerStartCommand";
    }

}
