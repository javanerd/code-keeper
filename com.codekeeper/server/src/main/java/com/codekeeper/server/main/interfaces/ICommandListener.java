/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.main.interfaces;

import java.util.Timer;

/**
 * Listen for commands.
 *
 * @author fredladeroute
 */
public interface ICommandListener {

    /**
     * Start the underlying timer task and listen for a new command.
     */
    void start();

    /**
     * Stop the underlying timer task.
     */
    void stop();

    /**
     * Restart the command listener by calling stop() then start().
     */
    void restart();

    /**
     * Return a reference to the timer for this Command listener.
     *
     * @return the underlying timer instance.
     */
    Timer getTimer();

    /**
     * Run a server command on this listener.
     *
     * @param command
     *            the server command to be executed.
     */
    void runCommand(ICommand command);
}
