/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.filters;

import org.glassfish.grizzly.filterchain.BaseFilter;
import org.glassfish.grizzly.filterchain.FilterChainContext;
import org.glassfish.grizzly.filterchain.NextAction;
import org.glassfish.grizzly.utils.Pair;

import com.codekeeper.server.enums.ServerMessage;

/**
 * Fourth filter in chain, managed the decryption of the input string.<br>
 * <br>
 * 1 LoggingFilter<br>
 * 2 AuthenticationFilter<br>
 * 3 PairingFilter<br>
 * 4 [DecryptionFilter]<br>
 * 5 XmlFilter<br>
 * 6 StorageFilter<br>
 * @author fredladeroute
 */
public class DecryptionFilter extends BaseFilter {

    /**
     * Read message from the chain and decrypt the message, set the message to
     * be the un-encrypted XML data.
     *
     * @param ctx
     *            FilterChainContext value.
     * @return the filter chain context.
     */
    @Override
    public final NextAction handleRead(final FilterChainContext ctx) {

        final Pair<ServerMessage, String> pair =
                ctx.<Pair<ServerMessage, String>> getMessage();

        switch (pair.getFirst()) {
            case DECRYPTFAIL:
                break;
            case DECRYPTPASS:
                break;
            case HANDSHAKE:
                break;
            case MD5FAIL:
                break;
            case MD5PASS:
                break;
            case NOMSG:
                break;
            case OK:
                break;
            case RAWDATA:
                break;
            case REQUEST_RETRANSMIT:
                break;
            case UNKNOWNERROR:
                break;
            default:
                break;
        }
        pair.getSecond();

        return ctx.getForkAction();
    }

}
