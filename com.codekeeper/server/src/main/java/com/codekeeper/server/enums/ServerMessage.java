/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.enums;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.server.exceptions.ServerException;
import com.codekeeper.shared.util.Constants;

/**
 * @author fredladeroute
 */
public enum ServerMessage {

    /**
     * Unknown error state. Signal to the next filter that an unknown error
     * occured. int value = 0
     */
    UNKNOWNERROR(0),

    /**
     * Message digest failure. Signal to the next filter that the message
     * failed MD5 checking. int value = 1
     */
    MD5FAIL(1),

    /**
     * Message digest pass. int value = 2
     */
    MD5PASS(2),

    /**
     * Message is ok. Signal to the next filter that the message is OK. int
     * value = 3
     */
    OK(3),

    /**
     * Decryption failed. Signal to inform that next filter that the previous
     * decryption failed. int value = 4
     */
    DECRYPTFAIL(4),

    /**
     * Decryption passed. int value = 5
     */
    DECRYPTPASS(5),

    /**
     * Signal to the filter that the data is raw data from a client. int value
     * = 6
     */
    RAWDATA(6),

    /**
     * Signal client that message is bad and we require a retransmission of the
     * message. int value = 7
     */
    REQUEST_RETRANSMIT(7),

    /**
     * Signal client the initiation of a diffie hullman handshake. int value =
     * 8
     */
    HANDSHAKE(8),

    /**
     * Not a message. int value = 9
     */
    NOMSG(9);

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ServerMessage.class);

    /**
     * Ordinal value for this enum.
     */
    private int ord;

    /**
     * Private constructor.
     *
     * @param ordinal
     *            the ordinal value for the instance of this enum
     */
    private ServerMessage(final int ordinal) {
        if (ordinal < Constants.SERVERMESSAGE_ORD_SIZE) {
            this.ord = ordinal;
        } else {
            throw new ServerException("Ordinal out of bounds.");
        }
    }

    /**
     * Return the value of a specific enum value.
     *
     * @return the enum value
     */
    public int value() {
        return this.ord;
    }

    /**
     * Return the value as bytes.
     *
     * @return byte array containing this value
     */
    public byte[] getAsBytes() {
        try {
            return ("" + ord).getBytes(Constants.ENCODING);
        } catch (final UnsupportedEncodingException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }
}
