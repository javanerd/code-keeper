package com.codekeeper.server.filters;

import org.glassfish.grizzly.filterchain.BaseFilter;
import org.glassfish.grizzly.filterchain.FilterChainContext;
import org.glassfish.grizzly.filterchain.NextAction;
import org.glassfish.grizzly.utils.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.server.enums.ServerMessage;

/**
 * The second filter used to setup a client to server<br>
 * session.<br>
 * <br>
 * 1 LoggingFilter<br>
 * 2 AuthenticationFilter<br>
 * 3 PairingFilter<br>
 * 4 [CommunicationFilter]<br>
 * 5 DecryptionFilter<br>
 * 6 XmlFilter<br>
 * 7 StorageFilter<br>
 * <br>
 *
 * @author fredladeroute
 */
public class CommunicationFilter extends BaseFilter {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(CommunicationFilter.class);

    /**
     * Handle read.
     *
     *@param ctx
     *     filterchaincontext object.
     * @return
     *     NextAction in filterchain.
     */
    public final NextAction handleRead(final FilterChainContext ctx) {

        final Pair<ServerMessage, String> p =
                ctx.<Pair<ServerMessage, String>>getMessage();
        final ServerMessage m = p.getFirst();

        switch (m) {
            case RAWDATA:
                LOGGER.debug("RAWDATA Recieved.");
                LOGGER.debug("Processing Next Filter in chain.");
                //Goto next filter in chain
                return ctx.getInvokeAction();
            case DECRYPTFAIL:
                ctx.write(ctx.getAddress(),
                        ServerMessage.DECRYPTFAIL.getAsBytes(),
                        null);
                break;
            case MD5FAIL:
                ctx.write(ctx.getAddress(),
                        ServerMessage.MD5FAIL.getAsBytes(),
                        null);
                break;
            case OK:
                break;
            case UNKNOWNERROR:
                // write REQUEST_RETRANSMIT (7) back to client.
                LOGGER.debug("RAWDATA Not Recieved. "
                        + "Requesting re-transmission.");
                ctx.write(ctx.getAddress(),
                        ServerMessage.REQUEST_RETRANSMIT.getAsBytes(),
                        null);
                break;
            default:
                break;
        }

        LOGGER.debug("Stopping.");
        return ctx.getStopAction();
    }

}
