/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.policies;

import java.net.InetAddress;
import java.util.HashSet;

import com.codekeeper.shared.properties.PropertyManager;

/**
 * Default address validation policy class. Rejects connections defined in
 * properties file under:
 *
 * <pre>
 * com.codekeeper.ip.blocklist.default
 * </pre>
 *
 * @author fredladeroute
 */
public class AddressValidationPolicyDefault implements
        IAddressValidationPolicy {

    /**
     * List of IP Addresses to block.
     */
    private static HashSet<String> blockList = PropertyManager.getInstance()
            .getStringPropertyAsHashSet("com.codekeeper.ip.blocklist.default");

    @Override
    public final boolean validateAddress(final InetAddress inetAddr) {
        final String addr = inetAddr.getHostAddress();
        return !blockList.contains(addr);
    }

    /**
     * @return the blockList
     */
    public static HashSet<String> getBlockList() {
        return blockList;
    }

    /**
     * @param blockList1
     *            the blockList to set
     */
    public static void setBlockList(final HashSet<String> blockList1) {
        AddressValidationPolicyDefault.blockList = blockList1;
    }

}
