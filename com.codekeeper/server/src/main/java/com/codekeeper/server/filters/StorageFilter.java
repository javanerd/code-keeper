/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.filters;

import java.io.IOException;

import org.glassfish.grizzly.filterchain.BaseFilter;
import org.glassfish.grizzly.filterchain.FilterChainContext;
import org.glassfish.grizzly.filterchain.NextAction;

/**
 * Storage filter last filter in the chain, responsible<br>
 * for storing the data from the message into the database.<br>
 * <br>
 * 1 LoggingFilter<br>
 * 2 AuthenticationFilter<br>
 * 3 PairingFilter<br>
 * 4 DecryptionFilter<br>
 * 5 XmlFilter<br>
 * 6 [StorageFilter]<br>
 * <br>
 *
 * @author fredladeroute
 */
public class StorageFilter extends BaseFilter {

    /**
     * Handle just read operation, when some message has come and ready to be
     * processed.
     *
     * @param ctx
     *            Context of {@link FilterChainContext} processing
     * @return the next action
     * @throws java.io.IOException
     *             when an error occurs
     */
    @Override
    public final NextAction handleRead(final FilterChainContext ctx)
            throws IOException {

        //TODO Get message, convert to XML then store the XML Document.
        ctx.getMessage();
        return ctx.getInvokeAction();
    }

}
