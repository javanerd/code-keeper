/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.main.commands;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.server.main.interfaces.ICommand;

/**
 * Abstract server command class.
 *
 * @author fredladeroute
 */
public abstract class ServerCommand implements ICommand, ILoggable {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ServerCommand.class);

    @Override
    public abstract boolean execute();

    /**
     * Get the name of this server command.
     *
     * @return the name of this server command
     */
    public abstract String getName();

    /**
     * Log the name of the command.
     */
    @Override
    public final void log() {
        LOGGER.debug("Executing server command: " + getName());
    }

}
