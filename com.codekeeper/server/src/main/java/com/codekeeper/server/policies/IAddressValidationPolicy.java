/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.policies;

import java.net.InetAddress;

/**
 * Defines an address validation policy.
 *
 * @author fredladeroute
 */
public interface IAddressValidationPolicy {

    /**
     * Validate the address.
     *
     * @param inetAddr
     *            inet address to validate
     * @return true if the address is valid false otherwise.
     */
    boolean validateAddress(InetAddress inetAddr);
}
