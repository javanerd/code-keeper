/*******************************************************************************
 * Copyright (c) 2014 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.server.security.tests;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.Random;

import junit.framework.TestCase;

import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.server.security.AESCrypto;
import com.codekeeper.shared.util.Constants;

/**
 * JUnit test for AESCrypto.java class.
 *
 * @author javanerd
 *
 */
public final class AESCryptoTest extends TestCase {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(AESCryptoTest.class);

    /**
     * Test password.
     */
    private static final String PASSWORD =
            "234erewwe234";

    /**
     * Secure random instance.
     */
    private static final Random SRAND = new SecureRandom();

    /**
     * The size of the salt.
     */
    private static final int SALT_LEN = 32;

    /**
     * Salt.
     */
    private static final byte[] SALT = new byte[SALT_LEN];

    /**
     * AESCrypto instance.
     */
    private static AESCrypto aesCrypto;

    /**
     * Setup the test constants.
     * @throws Exception
     *         if an error occurs.
     */
    protected static void setUpBeforeClass() throws Exception {
        SRAND.nextBytes(SALT);
        aesCrypto = new AESCrypto(PASSWORD, SALT);
    }

    /**
     * Test AESCrypto() Ctor.
     */
    public void testAesCrypto() {
        aesCrypto = null;
        SRAND.nextBytes(SALT);
        aesCrypto = new AESCrypto(PASSWORD, SALT);
        assertNotNull(aesCrypto);
    }

    /**
     * Test AESCrypto.encrypt(byte[] data) method.
     *
     * @throws UnsupportedEncodingException
     *         If encoding is not supported.
     */
    public void testEncrypt() throws UnsupportedEncodingException {
        String inString = new String(
                "DataTestStringOmgCrazyDataStringVeryLongAndVeryBoring..."
                .getBytes(),
                Constants.ENCODING);
        LOGGER.debug("DATA-IN: " + inString);

        SRAND.nextBytes(SALT);

        //Encrypt
        aesCrypto = new AESCrypto(PASSWORD, SALT);
        byte[] ciphertext = aesCrypto.encrypt(
                Hex.encode(inString.getBytes()));

        LOGGER.debug("Cipher Text: "
            + new String(Hex.encode(ciphertext), Constants.ENCODING));

        //Decrypt
        byte[] data = Hex.decode(aesCrypto.decrypt(ciphertext));
        String outString = new String(data, Constants.ENCODING);

        LOGGER.debug("DATA-OUT: " + outString);
        assertEquals(inString, outString);
    }

}
