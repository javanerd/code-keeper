/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.client.network.filters;

import java.io.IOException;

import javax.net.ssl.SSLEngine;

import org.glassfish.grizzly.Connection;
import org.glassfish.grizzly.EmptyCompletionHandler;
import org.glassfish.grizzly.filterchain.BaseFilter;
import org.glassfish.grizzly.filterchain.FilterChainContext;
import org.glassfish.grizzly.filterchain.NextAction;
import org.glassfish.grizzly.ssl.SSLFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Client filter is responsible for redirecting server response to the standard
 * output.
 */
public class ClientFilter extends BaseFilter {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(BaseFilter.class);

    /**
     * Connection complete message.
     */
    protected static final String MESSAGE = "Connection Successful.";

    /**
     * SSL Filter instance.
     */
    private final SSLFilter sslFilter;

    /**
     * Create a SendMessageFilter instance.
     *
     * @param sslfilter
     *         instance of the sslfilter object.
     */
    public ClientFilter(final SSLFilter sslfilter) {
        this.sslFilter = sslfilter;
    }

    @Override
    public final NextAction handleConnect(final FilterChainContext ctx)
            throws IOException {
        final Connection<?> connection = ctx.getConnection();

        // Execute async SSL handshake
        sslFilter.handshake(
                connection, new EmptyCompletionHandler<SSLEngine>() {

            // Once SSL handshake will be completed - send greeting message
            @Override
            public void completed(final SSLEngine result) {
                // Here we send String directly
                connection.write(MESSAGE);
            }
        });

        return ctx.getInvokeAction();
    }

    /**
     * Handle just read operation, when some message has come and ready to be
     * processed.
     *
     * @param ctx
     *            Context of {@link FilterChainContext} processing
     * @return the next action
     * @throws java.io.IOException
     *      on IOException
     */
    @Override
    public final NextAction handleRead(final FilterChainContext ctx)
            throws IOException {
        // We get String message from the context, because we rely prev. Filter
        // in chain is StringFilter
        final String serverResponse = (String) ctx.getMessage();

        LOGGER.debug(serverResponse);
        return ctx.getStopAction();
    }
}
