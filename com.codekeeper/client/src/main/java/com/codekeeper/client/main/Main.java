/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.client.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.glassfish.grizzly.Connection;

import com.codekeeper.client.network.NetworkManager;

/**
 * Main class.
 * @author javanerd
 *
 */
public final class Main {

    /**
     * unused.
     */
    private Main() {
    }

    /**
     * Main.
     * @param args
     *      Main args
     * @throws IOException
     *        If buffered reader throws an IOException.
     */
    public static void main(final String[] args) throws IOException {

        final NetworkManager nm = new NetworkManager();
        Connection<?> c = nm.connect();

        System.out.println("Ready... (\"q\" to exit)");
        final BufferedReader inReader = new BufferedReader(
                new InputStreamReader(System.in));
        do {
            final String userInput = inReader.readLine();
            if (userInput == null || "q".equals(userInput)) {
                break;
            }

            c.write(userInput);
        } while (true);
    }
}
