/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.client.network;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.glassfish.grizzly.Connection;
import org.glassfish.grizzly.filterchain.FilterChainBuilder;
import org.glassfish.grizzly.filterchain.TransportFilter;
import org.glassfish.grizzly.nio.transport.TCPNIOTransport;
import org.glassfish.grizzly.nio.transport.TCPNIOTransportBuilder;
import org.glassfish.grizzly.ssl.SSLContextConfigurator;
import org.glassfish.grizzly.ssl.SSLEngineConfigurator;
import org.glassfish.grizzly.ssl.SSLFilter;
import org.glassfish.grizzly.utils.StringFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.client.network.filters.ClientFilter;
import com.codekeeper.shared.util.Constants;

/**
 * Network connection manager for client.
 * @author javanerd
 *
 */
public final class NetworkManager {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(NetworkManager.class);

    /**
     * Create a connection holder.
     */
    private volatile Connection<?> connection = null;

    /**
     * Create a FilterChain using FilterChainBuilder.
     */
    private FilterChainBuilder filterChainBuilder = null;

    /**
     * Static transport object.
     */
    private volatile TCPNIOTransport transport;

    /**
     * Create a new NetworkManager.
     */
    public NetworkManager() {
        filterChainBuilder = FilterChainBuilder.stateless();

        transport = TCPNIOTransportBuilder
                 .newInstance().build();
        // Add TransportFilter, which is responsible
        // for reading and writing data to the connection
        filterChainBuilder.add(new TransportFilter());

        // Initialize and add SSLFilter
        final SSLEngineConfigurator serverConfig = initializeSSL();
        final SSLEngineConfigurator clientConfig =
                 serverConfig.copy().setClientMode(true);

        final SSLFilter sslFilter = new SSLFilter(serverConfig, clientConfig);
        filterChainBuilder.add(sslFilter);

        // StringFilter is responsible for Buffer <-> String conversion
        filterChainBuilder.add(new StringFilter(Charset
                .forName(Constants.ENCODING)));

        // ClientFilter is responsible for redirecting server responses to the
        // standard output
        filterChainBuilder.add(new ClientFilter(sslFilter));
        transport.setProcessor(filterChainBuilder.build());
    }

    /**
     * Create an asynchronous connection to the server.
     *
     * @return the connection
     */
    public Connection<?> connect() {

        try {
            // start the transport
            transport.start();

            // perform async. connect to the server
            @SuppressWarnings("rawtypes")
            Future<Connection> future = transport.connect(Constants.HOST,
                    Constants.PORT);
            LOGGER.debug("Attempting connection to server: "
                    + Constants.HOST + ":" + Constants.PORT);
            // wait for connect operation to complete
            connection = future.get(Constants.CLIENT_CONN_TIMEOUT,
                    TimeUnit.SECONDS);
            if (connection.isOpen()) {
                LOGGER.debug("Connection Successful.");
            } else {
                LOGGER.debug("Connection Unsuccessful.");
            }
            assert connection != null;
            return connection;
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (ExecutionException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (TimeoutException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * Close the connection and stop the transport.
     *
     * @return true if the connection was closed successfully.
     */
    public boolean disconnect() {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
            transport.stop();
            return true;
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return false;
    }

    /**
     * Initialize the SSL engine.
     * @return the SSLEngineConfigurator
     */
    private static SSLEngineConfigurator initializeSSL() {
        // Initialize SSLContext configuration
        SSLContextConfigurator sslContextConfig = new SSLContextConfigurator();

        // Set key store
        ClassLoader cl = NetworkManager.class.getClassLoader();
        URL cacertsUrl = cl.getResource("ssltest-cacerts.jks");
        if (cacertsUrl != null) {
            sslContextConfig.setTrustStoreFile(cacertsUrl.getFile());
            sslContextConfig.setTrustStorePass("changeit");
        }

        // Set trust store
        URL keystoreUrl = cl.getResource("ssltest-keystore.jks");
        if (keystoreUrl != null) {
            sslContextConfig.setKeyStoreFile(keystoreUrl.getFile());
            sslContextConfig.setKeyStorePass("changeit");
        }

        // Create SSLEngine configurator
        return new SSLEngineConfigurator(sslContextConfig.createSSLContext(),
                false, false, false);
    }
}
