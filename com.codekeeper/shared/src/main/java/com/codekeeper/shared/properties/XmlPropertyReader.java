/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.shared.interfaces.IPropertyReader;

/**
 * XML Property Reader.
 * @author fredladeroute
 *
 */
public class XmlPropertyReader extends PropertyReader
            implements IPropertyReader {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER =
                LoggerFactory.getLogger(XmlPropertyReader.class);

    /**
     * XML Property Reader Ctor.
     *
     * @param filename
     *      Name of the file to read XML properties from.
     *
     * @throws IOException
     *      If the creating of the XML file fails.
     */
    public XmlPropertyReader(final String filename) throws IOException {
        super(new Properties());
        File file = new File(filename);
        file.createNewFile();
        setInputStream(new FileInputStream(file));
    }

    @Override
    public final void readProperties() throws IOException {
        LOGGER.debug("Reading XMLProperty file.");
        getProperties().loadFromXML(getInputStream());
    }

}
