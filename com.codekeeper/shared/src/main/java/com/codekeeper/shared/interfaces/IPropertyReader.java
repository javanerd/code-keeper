/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.interfaces;

import java.io.IOException;

/**
 * IPropertyReader defines the behavior of
 * a PropertyWriter.
 *
 * @author fredladeroute
 *
 */
public interface IPropertyReader {

    /**
     * Read properties from the input stream.
     *
     * @throws IOException if the underlying read buffer fails
     */
    void readProperties() throws IOException;
}
