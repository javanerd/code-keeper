/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.properties;

import java.io.FileNotFoundException;

import com.codekeeper.shared.abstractions.GenericPropertyManager;
import com.codekeeper.shared.interfaces.IPropertyReader;
import com.codekeeper.shared.interfaces.IPropertyWriter;
import com.codekeeper.shared.util.IntegerTransformer;

/**
 * Integer PropertyManager.
 * @author fredladeroute
 *
 */
public class IntegerPropertyManager extends GenericPropertyManager<Integer> {

    /**
     * Integer PropertyManager.
     * @param iPropertyWriter the PropertyWriter to write properties
     * @param iPropertyReader PropertyReader to read properties
     */
    public IntegerPropertyManager(final IPropertyWriter iPropertyWriter,
            final IPropertyReader iPropertyReader) {
        super(iPropertyWriter, iPropertyReader);
    }

    /**
     * Integer PropertyManager.
     * @param fileName the filename to use for the property reader and writer.
     * @throws FileNotFoundException if the file fileName cannot be found
     */
    public IntegerPropertyManager(final String fileName)
            throws FileNotFoundException {
        super(new FilePropertyWriter(fileName),
                new FilePropertyReader(fileName));
    }


    @Override
    public final Integer getProperty(final String key) {
        return getProperty(key, new IntegerTransformer());
    }

    @Override
    public final Integer getProperty(final String key,
            final Integer defaultValue) {
        return getProperty(key, defaultValue, new IntegerTransformer());
    }

}
