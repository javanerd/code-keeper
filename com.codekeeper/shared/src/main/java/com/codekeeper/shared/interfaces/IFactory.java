/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.interfaces;

/**
 * Interface for a factory class.
 *
 * @author fredladeroute
 *
 * @param <T> type of class for this IFactory to return
 */
public interface IFactory<T> {

    /**
     * Get an instance of type <T> from this Factory.
     *
     * @return instance of type <T>
     */
    T getInstance();

}
