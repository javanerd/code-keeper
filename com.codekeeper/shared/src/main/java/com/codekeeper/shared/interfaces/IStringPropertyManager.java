/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.interfaces;

import java.util.HashSet;
import java.util.List;

/**
 * Defines a property manager which can read and write string properties.
 * @author fredladeroute
 *
 */
public interface IStringPropertyManager {

    /**
     *
     * @param key
     *      the key of the property value to get
     *
     * @param defaultValue
     *      the default value to use if the value cannot be returned
     *
     * @return
     *      the value of key from the properties file or,
     * the default value if the value is null or zero length
     * or if an exception occurs
     */
    String getStringProperty(String key, String defaultValue);

    /**
     * Return a string property from the underlying PROPS instance.
     *
     * @param key
     *            the key of the value to get
     * @return the value assigned the key key
     */
    String getStringProperty(String key);

    /**
     * Add a string property to this property manager.
     * @param key the key of the value to add
     * @param value the string value for the provided key
     */
    void addProperty(String key, String value);

    /**
     * Return a list of property items. The delimiter must be defined in the
     * properties file with the key "delimiter" the default if this key is not
     * found is ";".
     *
     * @param key
     *      the key of the list
     *
     * @return a list of property items.
     */
    List<String> getStringPropertyAsList(String key);

    /**
     * Return a {@link java.util.HashSet} of property items.
     * The delimiter must be defined in the
     * properties file with the key "delimiter" the default if this key is not
     * found is ";".
     *
     * @param key
     *      the key of the list
     *
     * @return a list of property items.
     */
    HashSet<String> getStringPropertyAsHashSet(String key);

}
