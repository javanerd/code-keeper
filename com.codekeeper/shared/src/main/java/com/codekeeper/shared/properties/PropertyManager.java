/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.properties;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.shared.interfaces.IPropertyManager;
import com.codekeeper.shared.interfaces.IStringPropertyManager;
import com.codekeeper.shared.util.Constants;

/**
 * Property Manager.
 *
 * @author fredladeroute
 *
 */
public final class PropertyManager implements IStringPropertyManager {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER =
                LoggerFactory.getLogger(PropertyManager.class);

    /**
     * The property manager to read/write properties to/from.
     */
    private IPropertyManager<? extends Number> manager;

    /**
     * Local PropertyManager instance.
     */
    private static PropertyManager instance;

    /**
     * PropertyManager constructor.
     *
     * Private, unused.
     */
    private PropertyManager() {
        try {
            manager = new FloatPropertyManager(Constants.DEFAULT_PROPERTY_FILE);
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Singleton constructor.
     *
     * @return an instance of this property mananger.
     */
    public static PropertyManager getInstance() {
        if (instance == null) {
            instance = new PropertyManager();
        }
        return instance;
    }

    @Override
    public String getStringProperty(
            final String key, final String defaultValue) {
        return manager.getStringProperty(key, defaultValue);
    }

    @Override
    public String getStringProperty(final String key) {
        return manager.getStringProperty(key);
    }

    @Override
    public void addProperty(final String key, final String value) {
        manager.addProperty(key, value);
    }

    @Override
    public List<String> getStringPropertyAsList(final String key) {
        return manager.getStringPropertyAsList(key);
    }

    @Override
    public HashSet<String> getStringPropertyAsHashSet(final String key) {
        return manager.getStringPropertyAsHashSet(key);
    }

}
