/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.util;

/**
 * Constants class.
 *
 * @author fredladeroute
 *
 */
public final class Constants {

    /**
     * unused.
     */
    private Constants() {
    }

    /**
     * Encoding.
     */
    public static final String ENCODING               = "UTF-8";

    /**
     * Cryptographic algorithm used for encryption.
     */
    public static final String CRYPTO_ALGORITHM
                = "PBEWITHSHA256AND128BITAES-CBC-BC";

    /**
     * Encryption key size for the Diffie-Hellman key exchange.
     */
    public static final int    AES_DH_KEYSIZE         = 1024;

    /**
     * Seven.
     */
    public static final int    SEVEN                  = 7;

    /**
     * Six.
     */
    public static final int    SIX                    = 6;

    /**
     * Five.
     */
    public static final int    FIVE                   = 5;

    /**
     * Four.
     */
    public static final int    FOUR                   = 4;

    /**
     * Two.
     */
    public static final int    TWO                    = 2;

    /**
     * One.
     */
    public static final int    ONE                    = 1;

    /**
     * Zero.
     */
    public static final int    ZERO                   = 0;

    /**
     * Eight.
     */
    public static final int    EIGHT                  = 8;

    /**
     * Maximum number of unique server messages available.
     */
    public static final int    SERVERMESSAGE_ORD_SIZE = 10;

    /**
     * Server host name.
     */
    public static final String HOST                   = "127.0.0.1";

    /**
     * Server port number.
     */
    public static final int    PORT                   = 7778;

    /**
     * The default client connection timeout value.
     */
    public static final long   CLIENT_CONN_TIMEOUT    = 10;

    /**
     *
     */
    public static final int    THREE                  = 3;

    /**
     * Server sleep time to reduce CPU consumption. In milliseconds.
     */
    public static final long SERVER_THREAD_SLEEPTIME = 50;

    /**
     * Server shutdown timeout in seconds.
     */
    public static final long SERVER_SHUTDOWN_TIMEOUT = 10;

    /**
     * Defualt properties file.
     */
    public static final String DEFAULT_PROPERTY_FILE = "config.properties";

    /**
     * Number of times to try a command before bailing out.
     *
     * For example if the command.execute() method
     * returns false 10 times then we bail out.
     */
    public static final int COMMAND_TIMEOUT = 10;

    /**
     * Command poll delay in milliseconds.
     */
    public static final long COMMAND_POLL_DELAY = 250;

}
