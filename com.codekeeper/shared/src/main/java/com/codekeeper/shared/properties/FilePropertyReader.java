/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * File Property Reader.
 * @author fredladeroute
 *
 */
public class FilePropertyReader extends PropertyReader {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(FilePropertyReader.class);

    /**
     * FilePropertyReader.
     * @param fileName the name of the file to read properties from
     * if the file does not exist it will be created.
     */
    public FilePropertyReader(final String fileName) {
        super(new Properties());
        File inputFile = new File(fileName);
        try {
            //Create new file if not already existing.
            inputFile.createNewFile();
            setInputStream(new FileInputStream(inputFile));
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Override
    public final void readProperties() throws IOException {
        getProperties().load(getInputStream());
        LOGGER.info("Properties read into memory.");
        LOGGER.debug("Properties:" + getProperties());
    }

}
