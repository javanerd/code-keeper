/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.properties;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import com.codekeeper.shared.interfaces.IPropertyWriter;

/**
 * Write properties to disk.
 * @author fredladeroute
 *
 */
public abstract class PropertyWriter implements IPropertyWriter {

    /**
     * Properties container.
     */
    private Properties properties;

    /**
     * The output stream to write properties to.
     */
    private OutputStream out;

    /**
     * Property writer constructor.
     * @param pProperties the properties to be written to disk
     */
    public PropertyWriter(final Properties pProperties) {
        this.setProperties(pProperties);
    }

    @Override
    public abstract void writeProperties() throws IOException;

    /**
     * @return the output stream
     */
    public final OutputStream getOutputStream() {
        return out;
    }

    /**
     * @param output the output stream to set
     */
    protected final void setOutputStream(final OutputStream output) {
        this.out = output;
    }

    /**
     * @return the properties
     */
    public final Properties getProperties() {
        return properties;
    }

    /**
     * @param pProperties the properties to set
     */
    public final void setProperties(final Properties pProperties) {
        this.properties = pProperties;
    }

}
