/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.properties;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import com.codekeeper.shared.util.Constants;

/**
 * XmlPropertyWriter, write properties to file.
 * @author javanerd
 *
 */
public final class XmlPropertyWriter extends PropertyWriter {

    /**
     * XmlPropertyWriter constructor.
     * @param filename
     *       Name of the property file.
     * @throws IOException
     *       If the file cannot be created.
     */
    public XmlPropertyWriter(final String filename) throws IOException {
        super(new Properties());
        File file = new File(filename);
        file.createNewFile();
        setOutputStream(new FileOutputStream(file));
    }

    @Override
    public void writeProperties() throws IOException {
        getProperties().storeToXML(getOutputStream(), null, Constants.ENCODING);
    }

}
