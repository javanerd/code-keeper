/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.util;

import com.codekeeper.shared.interfaces.ITransformer;

/**
 * Transform an integer to a string.
 * @author fredladeroute
 *
 */
public class IntegerTransformer implements ITransformer<String, Integer> {

    @Override
    public final Integer transform(final String input) {
        return Integer.parseInt(input);
    }

}
