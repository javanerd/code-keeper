package com.codekeeper.shared.properties;

import java.io.IOException;

import com.codekeeper.shared.abstractions.GenericPropertyManager;

/**
 * XmlPropertyManager class.
 * @author javanerd
 *
 * @param <T>
 *             the type of XmlPropertyManager.
 */
public final class XmlPropertyManager<T extends Number>
                extends GenericPropertyManager<T> {

    /**
     * XmlPropertyManager constuctor.
     * @param filename
     *          the name of the xml property file.
     * @throws IOException
     *             if the file cannot be created.
     */
    public XmlPropertyManager(final String filename) throws IOException {
        super(new XmlPropertyWriter(filename), new XmlPropertyReader(filename));
    }

    @SuppressWarnings("unchecked")
    @Override
    public T getProperty(final String key) {
        return (T) getProperty(key, new NumberTransformer());
    }

    @Override
    public T getProperty(final String key, final T defaultValue) {
        T property = getProperty(key);
        if (property == null) {
            return defaultValue;
        } else {
            return property;
        }
    }

}
