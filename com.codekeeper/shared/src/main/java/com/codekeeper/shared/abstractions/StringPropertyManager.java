/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.abstractions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.shared.interfaces.IPropertyManager;
import com.codekeeper.shared.interfaces.IPropertyReader;
import com.codekeeper.shared.interfaces.IPropertyWriter;
import com.codekeeper.shared.interfaces.IStringPropertyManager;
import com.codekeeper.shared.interfaces.ITransformer;
import com.codekeeper.shared.properties.NumberTransformer;

/**
 * StringPropertyManager enable basic reading and writing of
 * String properties.
 *
 * @author fredladeroute
 *
 * @param <T> The type of property manager.
 *
 */
public abstract class StringPropertyManager<T extends Number>
        implements IStringPropertyManager, IPropertyManager<T> {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(StringPropertyManager.class);

    /**
     * Property instance.
     */
    private final Properties properties = new Properties();

    /**
     * True if the property file has been loaded into memory.
     */
    private boolean isLoaded = false;

    /**
     * The property writer to write properties to.
     */
    private IPropertyWriter propertyWriter;

    /**
     * The property reader to read properties from.
     */
    private IPropertyReader propertyReader;

    /**
     * Private PropertyManager constructor, not used.
     * @param iPropertyReader the property reader to read property values from
     * @param iPropertyWriter the property writer to write property values to
     */
    public StringPropertyManager(final IPropertyWriter iPropertyWriter,
            final IPropertyReader iPropertyReader) {

        if (iPropertyWriter == null) {
            throw new NullPointerException("IPropertyWriter cannot be null.");
        }

        if (iPropertyReader == null) {
            throw new NullPointerException("IPropertyReader cannot be null.");
        }

        this.propertyWriter = iPropertyWriter;
        this.propertyReader = iPropertyReader;
    }

    @Override
    public final String getStringProperty(
            final String key,
            final String defaultValue) {
        String temp;
        if (!isLoaded()) {
            try {
                getPropertyReader().readProperties();
                setLoaded(true);
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        temp = getProperties().getProperty(key);
        if ((key == null) || (key.length() <= 0)) {
            return defaultValue;
        } else if (temp == null || (temp.length() <= 0)) {
            return defaultValue;
        }
        return temp;
    }

    @Override
    public final String getStringProperty(final String key) {
        if (!isLoaded()) {
            try {
                getPropertyReader().readProperties();
                setLoaded(true);
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        if ((key == null) || (key.length() <= 0)) {
            throw new IllegalArgumentException("Key was null or zero length.");
        }
        return getProperties().getProperty(key);
    }

    @Override
    public final void addProperty(final String key, final String value) {

        try {
            if (!isLoaded()) {
                getPropertyReader().readProperties();
                setLoaded(true);
            }
            getProperties().put(key, value);
            getPropertyWriter().writeProperties();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Override
    public final List<String> getStringPropertyAsList(final String key) {
        return new ArrayList<String>(
                Arrays.asList(getStringProperty(key).split(
                    getStringProperty("delimiter", ";"))));
    }

    @Override
    public final HashSet<String> getStringPropertyAsHashSet(final String key) {
        return new HashSet<String>(
                Arrays.asList(getStringProperty(key).split(
                    getStringProperty("delimiter", ";"))));
    }

    /**
     * @return the propertyReader
     */
    protected final IPropertyReader getPropertyReader() {
        return propertyReader;
    }

    /**
     * @param iPropertyReader the propertyReader to set
     */
    protected final void setPropertyReader(
            final IPropertyReader iPropertyReader) {
        this.propertyReader = iPropertyReader;
    }

    /**
     * @return the propertyWriter
     */
    protected final IPropertyWriter getPropertyWriter() {
        return propertyWriter;
    }

    /**
     * @param iPropertyWriter the propertyWriter to set
     */
    protected final void setPropertyWriter(
            final IPropertyWriter iPropertyWriter) {
        this.propertyWriter = iPropertyWriter;
    }

    /**
     * @return the isLoaded
     */
    protected final boolean isLoaded() {
        return isLoaded;
    }

    /**
     * @param pIsLoaded the isLoaded to set
     */
    protected final void setLoaded(final boolean pIsLoaded) {
        this.isLoaded = pIsLoaded;
    }

    /**
     * @return the properties
     */
    protected final Properties getProperties() {
        return properties;
    }

    /**
     * Get type <T> using a transformer.
     *
     * @param key
     *      the key to the property
     *
     * @param transformer
     *      the transformer to transform key to value
     *
     * @return the value of type <T>
     */
    protected abstract T getProperty(String key,
            ITransformer<String, T> transformer);


    @Override
    public abstract T getProperty(String key);

    /**
     * Get type <T> using a transformer.
     *
     * @param key
     *      the key to the property
     *
     * @param transformer
     *      the transformer to transform key to value
     *
     * @param defaultValue
     *      the default value to use if something goes wrong
     *
     * @return the value of type <T>
     */
    protected abstract T getProperty(String key, T defaultValue,
            ITransformer<String, T> transformer);

    @Override
    public abstract T getProperty(String key, T defaultValue);

    @Override
    public abstract void addProperty(String key, T value);

    /**
     * Get property of type <T>.
     * @param key
     *         the key of the property.
     * @param transformer
     *         the property value transformer.
     * @return
     *         the property of type <T>
     */
    protected abstract Number getProperty(String key,
            NumberTransformer transformer);

}
