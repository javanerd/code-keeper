/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.interfaces;


/**
 * Defines a PropertyManager of type <T extends Number>
 * by default any IPropertyManager can read and write
 * String properties.
 *
 * @author fredladeroute
 *
 * @param <T> Defines the type of value which this
 * IPropertyManager can read and write.
 */
public interface IPropertyManager<T extends Number>
            extends IStringPropertyManager {

    /**
     * Add a property to this property manager.
     *
     * @param key
     *            the key to store the property under
     * @param value
     *            the value of the property to be added
     */
    void addProperty(String key, T value);

    /**
     * Get a property value given only a key.
     * @param key the key to the value
     * @return the value of key
     */
    T getProperty(String key);

    /**
     * Get a property value given only a key and
     * a default fall back value.
     * @param key
     *      the key to get the value of
     * @param defaultValue
     *      the default fall back value if
     *      something should go wrong
     * @return the value stored in the properties under the key key
     */
    T getProperty(String key, T defaultValue);

}
