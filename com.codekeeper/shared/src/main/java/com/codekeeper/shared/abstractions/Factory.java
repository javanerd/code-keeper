/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.abstractions;

import com.codekeeper.shared.interfaces.IFactory;


/**
 * Factory class.
 *
 * @author fredladeroute
 *
 * @param <T> type of factory
 *
 */
public abstract class Factory<T> implements IFactory<T> {

    /**
     * Holder for previous instance,
     * if we need to get the previously created class.
     */
    private T previousInstance;

    /**
     * Factory Ctor, unused.
     */
    public Factory() { }

    @Override
    public abstract T getInstance();

    /**
     * @return the previousInstance
     */
    public final T getPreviousInstance() {
        return previousInstance;
    }

    /**
     * @param pPreviousInstance the previousInstance to set
     * @return the input
     */
    protected final T setPreviousInstance(final T pPreviousInstance) {
        this.previousInstance = pPreviousInstance;
        return previousInstance;
    }

}
