package com.codekeeper.shared.properties;

import com.codekeeper.shared.abstractions.AbstractTransformer;

/**
 * Number transformer.
 * @author javanerd
 *
 */
public final class NumberTransformer extends
        AbstractTransformer<String, Number> {

    @Override
    public Number transform(final String input) {
        Number t = null;
        try {
            t = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            try {
                t = Double.parseDouble(input);
            } catch (NumberFormatException e1) {
                try {
                    t = Float.parseFloat(input);
                } catch (NumberFormatException e2) {
                    try {
                        t = Long.parseLong(input);
                    } catch (NumberFormatException e3) {
                        throw new NumberFormatException("Not a number.");
                    }
                }
            }
        }
        return t;
    }

}
