package com.codekeeper.shared.util;

/**
 * Util class.
 * @author javanerd
 *
 */
public final class Utils {

    /**
     * Unused ctor.
     */
    private Utils() {
    }
    /**
     * Clear an array from memory.
     * Then invoke garbage collector.
     *
     * @param arr
     *         the array to be cleared from memory.
     */
    public static void clear(final Object[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = null;
        }
        System.gc();
    }

    /**
     * Clear an array of bytes from memory.
     *
     * @param arr
     *         the array to be cleared.
     */
    public static void clear(final byte[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = 0x00;
        }
        System.gc();
    }
}
