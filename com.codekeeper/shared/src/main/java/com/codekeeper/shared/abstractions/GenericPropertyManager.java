/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.abstractions;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.shared.interfaces.IPropertyReader;
import com.codekeeper.shared.interfaces.IPropertyWriter;
import com.codekeeper.shared.interfaces.ITransformer;
import com.codekeeper.shared.properties.NumberTransformer;

/**
 * A Generic property manager to read and write properties of type
 * <T> as well as String properties.
 * @author fredladeroute
 *
 * @param <T> Type of property manager.
 */
public abstract class GenericPropertyManager<T extends Number>
            extends StringPropertyManager<T> {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(GenericPropertyManager.class);
    /**
     * Constructs a PropertyManager.
     * @param iPropertyWriter the PropertyWriter to write changes to
     * @param iPropertyReader the PropertyReader to read changes from
     */
    public GenericPropertyManager(final IPropertyWriter iPropertyWriter,
            final IPropertyReader iPropertyReader) {
        super(iPropertyWriter, iPropertyReader);
    }

    @Override
    protected final T getProperty(final String key,
            final ITransformer<String, T> transformer) {
        if (!isLoaded()) {
            try {
                getPropertyReader().readProperties();
                setLoaded(true);
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        if ((key == null) || (key.length() <= 0)) {
            throw new IllegalArgumentException("Key was null or zero length.");
        }
        return transformer.transform(getProperties().getProperty(key));
    }

    @Override
    protected final Number getProperty(final String key,
            final NumberTransformer transformer) {
        if (!isLoaded()) {
            try {
                getPropertyReader().readProperties();
                setLoaded(true);
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        if ((key == null) || (key.length() <= 0)) {
            throw new IllegalArgumentException("Key was null or zero length.");
        }
        return transformer.transform(getProperties().getProperty(key));
    }

    @Override
    protected final T getProperty(final String key,
            final T defaultValue, final ITransformer<String, T> transformer) {
        String temp;
        if (!isLoaded()) {
            try {
                getPropertyReader().readProperties();
                setLoaded(true);
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        temp = getProperties().getProperty(key);
        if ((key == null) || (key.length() <= 0)) {
            return defaultValue;
        } else if (temp == null || (temp.length() <= 0)) {
            return defaultValue;
        }

        return transformer.transform(temp);
    }

    @Override
    public final void addProperty(final String key, final T value) {
        try {
            if (!isLoaded()) {
                getPropertyReader().readProperties();
                setLoaded(true);
            }
            getProperties().put(key, value);
            getPropertyWriter().writeProperties();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
