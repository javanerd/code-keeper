/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.codekeeper.shared.interfaces.IPropertyReader;

/**
 * Read properties from an input stream.
 * @author fredladeroute
 *
 */
public abstract class PropertyReader implements IPropertyReader {

    /**
     * Properties to read from.
     */
    private Properties properties;

    /**
     * The input stream to read from.
     */
    private InputStream in;

    /**
     * PropertyReader constructor.
     *
     * @param pProperties
     *      the properties to read
     *
     */
    public PropertyReader(final Properties pProperties) {
        this.setProperties(pProperties);
    }

    @Override
    public abstract void readProperties() throws IOException;

    /**
     * @return the in
     */
    public final InputStream getInputStream() {
        return in;
    }

    /**
     * @param pIn the in to set
     */
    public final void setInputStream(final InputStream pIn) {
        this.in = pIn;
    }

    /**
     * @return the properties
     */
    public final Properties getProperties() {
        return properties;
    }

    /**
     * @param pProperties the properties to set
     */
    public final void setProperties(final Properties pProperties) {
        this.properties = pProperties;
    }

}
