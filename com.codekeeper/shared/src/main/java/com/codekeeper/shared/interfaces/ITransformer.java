/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.interfaces;

/**
 * Transform input type to output type.
 *
 * @author fredladeroute
 *
 * @param <I> input type
 * @param <O> output type
 */
public interface ITransformer<I, O> {

    /**
     * Transform input to output.
     * @param input input to be transformed to type <O>
     * @return input transformed to type <O>
     */
    O transform(I input);
}
