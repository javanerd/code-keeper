package com.codekeeper.shared.abstractions;

import com.codekeeper.shared.interfaces.ITransformer;

/**
 * Abstract Transformer class.
 *
 * @author javanerd
 *
 * @param <I>
 *         input type.
 * @param <O>
 *         output type.
 */
public abstract class AbstractTransformer<I, O> implements ITransformer<I, O> {

    /**
     * Transform input to output.
     * @param input input to be transformed to type <O>
     * @return input transformed to type <O>
     */
    public abstract O transform(I input);
}
