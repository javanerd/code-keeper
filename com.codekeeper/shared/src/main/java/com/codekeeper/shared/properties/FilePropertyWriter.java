/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.properties;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codekeeper.shared.interfaces.IPropertyWriter;

/**
 *
 * @author fredladeroute
 *
 */
public class FilePropertyWriter extends PropertyWriter
                implements IPropertyWriter {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(PropertyWriter.class);

    /**
     * Property writer constructor.
     *
     * @param filename
     *      the name of the file to write properties to
     */
    public FilePropertyWriter(final String filename) {
        super(new Properties());
        File file = new File(filename);
        try {
            file.createNewFile();
            setOutputStream(new FileOutputStream(file));
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Override
    public final void writeProperties() throws IOException {
        getProperties().store(getOutputStream(), null);
        LOGGER.info("Properties written.");
    }

}
