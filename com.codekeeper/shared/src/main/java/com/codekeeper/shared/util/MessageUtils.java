/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.util;

/**
 * Message utils for parsing incoming messages.
 *
 * @author fredladeroute
 *
 */
public final class MessageUtils {

    /**
     * unused.
     */
    private MessageUtils() {
    }

    /**
     * Get the header of a message.<br>
     * <br>
     * The header format is as follows: <br>
     * <br>
     * Message Format:<br>
     * _______________________________________________<br>
     * |__16-bits___|___________Remainder____________|<br>
     * Header, Remaining message<br>
     * <br>
     *
     * In Java 1 char = 16-bits
     *
     * First Char of the string is the header it's value
     * is from [0 - 2^15-1] (int)
     * corrisponding to a value in the ServerMessage.java enum.
     *
     * @param message
     *            the entire message
     * @return the header string
     */
    public static synchronized int getHeader(final String message) {
        return message.charAt(0);
    }
}
