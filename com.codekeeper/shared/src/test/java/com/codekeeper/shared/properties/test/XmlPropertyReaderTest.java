/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.properties.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import com.codekeeper.shared.properties.XmlPropertyReader;
import com.codekeeper.shared.util.test.TestConstants;

/**
 * @author javanerd
 *
 */
public final class XmlPropertyReaderTest {

    /**
     * Temporary property file holder.
     */
    private static File propertyFile;

    /**
     * Setup property file.
     * @throws java.lang.Exception
     *         if an error occurs.
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        propertyFile = new File("/tmp/"
               + TestConstants.DEFAULT_XMLPROPERTY_FILE);
        propertyFile.createNewFile();
    }

    /**
     * @throws IOException
     *      If XmlPropertyReader cannot create file.
     *
     */
    @Test
    public void testCtor() throws IOException {
        XmlPropertyReader xpr = new XmlPropertyReader(
                propertyFile.getAbsolutePath());
        assertNotNull(xpr);
    }

    /**
     * @throws IOException
     *      If XmlPropertyReader cannot create file.
     */
    @Test
    public void testGetProperty() throws IOException {
        XmlPropertyReader xpr = new XmlPropertyReader(
                propertyFile.getAbsolutePath());
        xpr.getProperties().setProperty("testprop", "value");
        String prop = xpr.getProperties().getProperty("testprop");
        assertEquals(prop, "value");
    }

}
