/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.util.test;

/**
 * Constants class.
 *
 * @author fredladeroute
 *
 */
public final class TestConstants {

    /**
     * Unused Ctor.
     */
    private TestConstants() {
    }

    /**
     * Defualt properties file.
     */
    public static final String DEFAULT_PROPERTY_FILE = "test.properties";

    /**
     * Defualt xml properties file.
     */
    public static final String DEFAULT_XMLPROPERTY_FILE = "test.xml";
}
