/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying
 * materials are made available under the terms of the GNU
 * Public License v3.0 which accompanies this distribution,
 * and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *      Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.codekeeper.shared.properties.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.BeforeClass;
import org.junit.Test;

import com.codekeeper.shared.properties.PropertyReader;
import com.codekeeper.shared.properties.XmlPropertyReader;
import com.codekeeper.shared.util.Constants;

/**
 * Test PropertyReader class.
 * @author fredladeroute
 *
 */
public final class PropertyReaderTest {

    /**
     * Temporary property file holder.
     */
    private static File propertyFile;

    /**
     * Setup class.
     * @throws java.lang.Exception
     *         on error.
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        propertyFile = new File("/tmp/" + Constants.DEFAULT_PROPERTY_FILE);
        propertyFile.createNewFile();
    }

    /**
     * Test PropertyReader constructor.
     *
     * @throws IOException
     *       If XmlPropertyReader cannot create file.
     */
    @Test
    public void testCtor() throws IOException {
        PropertyReader pr = new XmlPropertyReader(
                propertyFile.getAbsolutePath());
        assertTrue(propertyFile.getAbsolutePath().contains(
                Constants.DEFAULT_PROPERTY_FILE));
        assertNotNull(pr);
        assertNotNull(pr.getProperties());
    }

    /**
     * Test PropertyManager.getInputStream().
     */
    @Test
    public void testGetInputStream() {
        PropertyReader pr = null;
        try {
            pr = new XmlPropertyReader(propertyFile.getAbsolutePath());
        } catch (IOException e) {
            fail(e.getMessage());
        }
        InputStream is = pr.getInputStream();
        assertNotNull(is);
    }

}
